-------------- This is BETA version 0.59 -----------------------------------------

libxm7 - A library to play XM modules on Nintendo DS using only ARM7 resources

-------------- This is BETA version 0.59 -----------------------------------------

Documentation can be found on-line here:

http://www.teleion.it/users/cgq/nds/libxm7

----------------------------------------------------------------------------------

License: Noncommercial zlib license (see license.txt). Source code is not included.
If you need a different licensing, please contact me.

----------------------------------------------------------------------------------

Please address bug reports and questions to the e-mail address you can find at the
documentation web page.

2008-11-05, Sverx
