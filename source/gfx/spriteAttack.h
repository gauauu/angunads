
//{{BLOCK(spriteAttack)

//======================================================================
//
//	spriteAttack, 256x192@8, 
//	+ palette 256 entries, not compressed
//	+ 211 tiles (t|p reduced) not compressed
//	+ regular map (flat), not compressed, 32x24 
//	Total size: 512 + 13504 + 1536 = 15552
//
//	Time-stamp: 2008-11-07, 10:25:15
//	Exported by Cearn's GBA Image Transmogrifier
//	( http://www.coranac.com )
//
//======================================================================

#ifndef __SPRITEATTACK__
#define __SPRITEATTACK__

#define spriteAttackPalLen 512
extern const unsigned short spriteAttackPal[256];

#define spriteAttackTilesLen 13504
extern const unsigned short spriteAttackTiles[6752];

#define spriteAttackMapLen 1536
extern const unsigned short spriteAttackMap[768];

#endif // __SPRITEATTACK__

//}}BLOCK(spriteAttack)

