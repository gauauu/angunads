Anguna is an RPG'ish action-adventure homebrew game for Nintendo's Gameboy Advance and Nintendo DS systems. This repo contains the source code for the DS version.

Compiled builds, downloads, and more information are all available from [http://www.tolberts.net/anguna](http://www.tolberts.net/anguna)

Building Anguna
----------------
Anguna is designed to be built using devkitPro's libnds/devkitARM toolchain from 2008.
It uses a number of custom converters and perl scripts as well.



Prerequisites
-------------------

- DevkitPro's toolchain https://sourceforge.net/projects/devkitpro
	Unfortunately, devkitPro's tools aren't backwards compatible, and APIs have changed somewhat,
	so the standard method of installing devkitPro (the windows updater) may not work.
	Anguna was built using the July 2008 release of the various devkitPro products.

        Currently, a working build of devkitPro is bundled in the git repo.


- Perl, along with the Switch module

- If building in linux, you need dos2unix and wine as well

- If you want to edit level maps, you need Mappy http://www.tilemap.co.uk/mappy.php


Building
---------------
set the DEVKITPRO and DEVKITARM environment variables to point at your devkitPro installation. 
Run make. Cross your fingers and hope for the best.